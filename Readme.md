# Supadu Technical Test (PHP/WordPress)

> This is the technical test for our back-end (PHP) positions at Supadu.

Thank you for your interest in becoming part of Supadu. We would like you to complete the technical test below.

Before we start, we want to give you an idea of what we do and how your solution will fit in with our products. Our main product is a data management platform for publishers that allows them to manage and manipulate their book data. The platform provides an API which can be used to populate websites and apps with the publishers data and enables them to provide the ability to search their catalogs to their customers. We also assist our customers with the production of their websites and our platform of choice is WordPress.

This should take you roughly three hours. If you'd like to do more, but don't have time then please mention it in your README.

## The Challenge
The goal is to create a WordPress plugin that interacts with our API to display books on a page or post.

## Dont�s
- *Don't* use a plugin boilerplate
- *Don't* send un-tested code
- *Don't* put JS and CSS in the page source

## Do�s
- *Do* use appropriate packages as necessary
- *Do* use dependency management for used packages
- *Do* comment your code
- *Do* data validation 
- *Do* think about where errors are thrown and how they are handled during the operation of the plugin
- *Do* use and object oriented approach
- *Do* use good design patterns
- *Do* use consistent code styles
- *Do* elaborate on what you would do, given the time
- *Do* use Git and submit your test via a hosted Git Management tool e.g. BitBucket or GitHub


*So we would like for you to�*

1. Create an admin page which enables a user to set the API Key and Service URL to be used in the plugin
2. Give the plugin the ability to display the covers and titles of books obtained by querying the API, during page load, with a list of ISBNs
3. Give the plugin the ability to display the covers and titles of books obtained by querying the API with a list of ISBNs using AJAX
4. Complete steps 2 and 3 but using a collection instead of a list of ISBNs

Your solution needs to enable us to load the plugin into WordPress and easily display books on any page we create using either a collection or a list of ISBNs. A collection is a list of products that have been created using the data management dashboard and has an associated alias.

## Resources

- Service URL: http://folioservices.supadu.com/v2/
- API Key: 9819864f6ff0e8a5a097c99224cfd18a
- Docs: http://developer.supadu.com/supafolio.html (note API Key can be passed to the service by using x-apikey as a GET query string parameter although this is not mentioned in the docs.)
- ISBNs: 9781509815494, 9781509800254, 9781509842186, 9781509842148, 9781509829941
- Collection Alias: science-fiction-essentials


## Test Considerations

Last but not least, please make sure you test your code and make it as easy to install and use as possible (i.e. provide a descriptive and well-formatted README).

Good luck!